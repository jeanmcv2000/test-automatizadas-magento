import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class setUp {
	
	WebDriver driver;
	
	@BeforeEach
	void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\juan.casas\\Pictures\\e-commerce-solution\\MagentoProject1\\chromedriver.exe");   
		driver = new ChromeDriver();
		Thread.sleep(4000);
	    driver.get("http://demo-ecommerce-solution01.southcentralus.cloudapp.azure.com:8082/"); 
	}

	//@Test
	void test() throws InterruptedException {

	   	   String shoppingCart="//a[@class='action showcart']";
	   	   WebElement element=driver.findElement(By.xpath(shoppingCart));	   	   
	   	   
	       element.click();
	       Thread.sleep(4000);
	       driver.close();
	                  
	}
	//@Test
	void createAccount() throws InterruptedException {
		//Variables
		 String createAccount="/html/body/div[2]/header/div[1]/div/ul/li[4]/a";
		 String name="//input[@name='firstname']";
		 String lastName="//input[@name='lastname']";
		 String email="//input[@id='email_address']";
		 String password="//input[@id='password']";
		 String confirmPsw="//input[@id='password-confirmation']";
		 String confirmAccount="//*[@id=\"form-validate\"]/div/div[1]/button/span";
		 
		 
		 
	   	 WebElement element=driver.findElement(By.xpath(createAccount));
	   	   //Thread.sleep(4000);
	       element.click();
	       Thread.sleep(2000);
	       
	     WebElement name1=driver.findElement(By.xpath(name));
	       name1.clear();
	       Thread.sleep(2000);
	       name1.sendKeys("Ana");
	       Thread.sleep(2000);
	       
	     WebElement lastname=driver.findElement(By.xpath(lastName));
	       lastname.clear();
	       Thread.sleep(2000);
	       lastname.sendKeys("Chessani");
	       Thread.sleep(2000);
	       
	     WebElement email1=driver.findElement(By.xpath(email));
	       email1.clear();
	       Thread.sleep(2000);
	       email1.sendKeys("chess.ana@hotmail.com");
	       Thread.sleep(2000);
	       
	     WebElement password1=driver.findElement(By.xpath(password));
	      	password1.clear();
	        Thread.sleep(2000);
	        password1.sendKeys("Chess-1995");
	        Thread.sleep(2000);
	        
	     WebElement confirmPsw1=driver.findElement(By.xpath(confirmPsw));
	      	confirmPsw1.clear();
	        Thread.sleep(2000);
	        confirmPsw1.sendKeys("Chess-1995");
	        Thread.sleep(2000);
	        
	      
	     WebElement confirmAccount1=driver.findElement(By.xpath(confirmAccount));
	      	confirmAccount1.click();
	      	Thread.sleep(2000);
		driver.close();
	}
	//@Test
	void storeLocatorCountry() throws InterruptedException {
		
		logInn();
	      	
		String foundShop="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String country="//*[@id=\"country\"]";
		
		WebElement foundShop1=driver.findElement(By.xpath(foundShop));
			foundShop1.click();
	        Thread.sleep(500); 
	    
	        WebElement country1=driver.findElement(By.xpath(country));
	        country1.click();
	        Thread.sleep(200);
	        country1.sendKeys("Chile");
	        Thread.sleep(500); 
	        click();
	        
	        driver.close();
	        
	}
	//@Test
	void storeLocatorCity() throws InterruptedException {
		
		logInn();
	      	
		String foundShop="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String city="//*[@id=\"city\"]";
		
		WebElement foundShop1=driver.findElement(By.xpath(foundShop));
			foundShop1.click();
	        Thread.sleep(500); 
	        
	        WebElement city1=driver.findElement(By.xpath(city));
	        city1.clear();
	        Thread.sleep(200);
	        city1.sendKeys("SANTIAGO");
	        Thread.sleep(500); 
	        click();
	        
	        driver.close();
	}
	//@Test
	void storeLocatorState() throws InterruptedException {
		
		logInn();
	      	
		String foundShop="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String state="//*[@id=\"state\"]";
		
		WebElement foundShop1=driver.findElement(By.xpath(foundShop));
			foundShop1.click();
	        Thread.sleep(500); 
      
	        WebElement state1=driver.findElement(By.xpath(state));
	        state1.clear();
	        Thread.sleep(200);
	        state1.sendKeys("SANTIAGO");
	        Thread.sleep(500); 
	        click();
	        
	        driver.close();

	}
	
	//@Test
	void storeLocatorPostalCode() throws InterruptedException {
		
		logInn();
	      	
		String foundShop="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String postalCode="//*[@id=\"zipcode\"]";
		
		WebElement foundShop1=driver.findElement(By.xpath(foundShop));
			foundShop1.click();
	        Thread.sleep(500); 
	    

	        
	        WebElement postalCode1=driver.findElement(By.xpath(postalCode));
	        postalCode1.clear();
	        Thread.sleep(200);
	        postalCode1.sendKeys("00000");
	        Thread.sleep(500); 
	        click();
	        
	        driver.close();
	        
	}
	
	@Test
	void layeredNavigation() throws InterruptedException {
		
		logInn();
	      	
		String category="/html/body/div[2]/div[1]/div/div[2]/nav/ul/li[1]/a/span";
		String openLayered="//*[@id=\"narrow-by-list\"]/div[1]/div[1]";
		String barLayered="//*[@id=\"lof_price_slider\"]";
		
			WebElement category1=driver.findElement(By.xpath(category));
			category1.click();
	        	Thread.sleep(500); 
	        
	        WebElement openLayered1=driver.findElement(By.xpath(openLayered));
	        openLayered1.click();
		        Thread.sleep(500); 
		        
		    WebElement barLayered1=driver.findElement(By.xpath(barLayered));
		    barLayered1.click();
			    Thread.sleep(500);
	        
	        driver.close();
	        
	}
	
	void click() throws InterruptedException {
		String search="/html/body/div[2]/main/div[3]/div/div[4]/div/div/div/form/div[3]/div/button/span";
		WebElement search1=driver.findElement(By.xpath(search));
        search1.click();
        Thread.sleep(4000); 
	}
	
	void logInn() throws InterruptedException {
		String logIn="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String user="//*[@id=\"email\" and @title=\"Correo electrónico\"]";
		String psw="//*[@id=\"pass\" and @title=\"Contraseña \"]";
		String enter="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[4]/div[1]/button/span";
		
		 WebElement logIn1=driver.findElement(By.xpath(logIn));
		 logIn1.click();
	      	Thread.sleep(500);
		
		WebElement user1=driver.findElement(By.xpath(user));
		   user1.clear();
	       Thread.sleep(500);
	       user1.sendKeys("chess.ana@hotmail.com");
	       Thread.sleep(2000);
	       
	    WebElement password1=driver.findElement(By.xpath(psw));
	      	password1.clear();
	        Thread.sleep(500);
	        password1.sendKeys("Chess-1995");
	        Thread.sleep(2000);
	        
	   WebElement enter1=driver.findElement(By.xpath(enter));
	   		enter1.click();
	      	Thread.sleep(500);
	}
	
}
