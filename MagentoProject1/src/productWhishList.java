import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class productWhishList {

	static WebDriver driver;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\juan.casas\\Pictures\\e-commerce-solution\\MagentoProject1\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo-ecommerce-solution01.southcentralus.cloudapp.azure.com:8082");
	}

	@Test
	void wishList() throws InterruptedException  {
		
		String login="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String email="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[2]/div/input";
		String password="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[3]/div/input";
		String submit="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[4]/div[1]/button";
		String category="//a[@id='ui-id-5']";
		String selectProduct="//*[@id=\"layer-product-list\"]/div[2]/ol/li[2]/div/a/span/span/img";
		String addProductList="//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[5]/div/a[1]/span";
		String selectWhishList="(//*[@id=\"wl-rd-13\"])[2]";
		String confirmProductList="//*[@id=\"mwl-continue\"]";
		String category2="//a[@id='ui-id-5']";
		String selectProduct2="//*[@id=\"layer-product-list\"]/div[2]/ol/li[6]/div/a/span/span/img";
		String addProductList2="//*[@id=\"maincontent\"]/div[2]/div/div[1]/div[5]/div/a[1]/span";
		String selectWhishList2="(//*[@id=\"wl-rd-13\"])[2]";
		String confirmProductList2="//*[@id=\"mwl-continue\"]";
		String myAccount="/html/body/div[2]/header/div[1]/div/ul/li[2]/span/button";
		String myWhishList="/html/body/div[2]/header/div[1]/div/ul/li[2]/div/ul/li[2]/a";
		String whishList="//*[@id=\"wl13\"]";
		String addWhishListToCart="//*[@id=\"wl-13\"]/div[2]/ol/div/form[2]/button";
		String shoppingCart="//a[@class='action showcart']";
		String viewShopCart="//*[@id=\"minicart-content-wrapper\"]/div[2]/div[5]/div/a/span";
		String selectBuyNow="//*[@id=\"maincontent\"]/div[3]/div/div[4]/div[1]/ul/li[1]/button";
		String continueShoping="//*[@id=\"shipping-method-buttons-container\"]/div/a";
		String editQuantity="//*[@id=\"shopping-cart-table\"]/tbody[1]/tr[2]/td/div/a[2]";
		String selectQuantity="//*[@id=\"qty\"]";
		String updateCart="//*[@id=\"product-updatecart-button\"]";
		String confirmBuy="//*[@id=\"maincontent\"]/div[3]/div/div[4]/div[1]/ul/li[1]/button";
		String shippingMethod="//*[@id=\"checkout-shipping-method-load\"]/table/tbody/tr[2]/td[1]/input";
		String showMap="/html/body/div[1]/main/div[2]/div/div[4]/div[4]/ol/li[2]/div/div[3]/form/div[1]/table/tbody/tr[3]/td/div/p/a";
		String selectStore="//*[@id=\"button-VICKU�A\"]";
		String submit2="//*[@id=\"shipping-method-buttons-container\"]/div/button";
		String paymentMethod="//*[@id=\"cashondelivery\"]";
		String submitToBuy="//*[@id=\"checkout-payment-method-load\"]/div/div/div[3]/div[2]/div[4]/div/button";
		
		WebElement login1=driver.findElement(By.xpath(login));
		login1.click();
		Thread.sleep(4000);
		
		WebElement email1=driver.findElement(By.xpath(email));
        email1.clear();
        email1.sendKeys("jeanmcv2000@gmail.com");
        Thread.sleep(4000);
        
        WebElement password1=driver.findElement(By.xpath(password));
        password1.clear();
        password1.sendKeys("admin_01");
        Thread.sleep(4000);
        
        WebElement submit1=driver.findElement(By.xpath(submit));
        submit1.click();
        Thread.sleep(4000);
        
        WebElement category1=driver.findElement(By.xpath(category));
		category1.click();
		Thread.sleep(4000);
		
		WebElement selectProduct1=driver.findElement(By.xpath(selectProduct));
        selectProduct1.click();
        Thread.sleep(4000);
        
		WebElement addProductList1=driver.findElement(By.xpath(addProductList));
        addProductList1.click();
        Thread.sleep(4000);
        
        WebElement selectWhishList1=driver.findElement(By.xpath(selectWhishList));
        selectWhishList1.click();
        Thread.sleep(4000);
        
        WebElement confirmProductList1=driver.findElement(By.xpath(confirmProductList));
        confirmProductList1.click();
        Thread.sleep(4000);
        
        WebElement category3=driver.findElement(By.xpath(category2));
		category3.click();
		Thread.sleep(4000);
        
        WebElement selectProduct3=driver.findElement(By.xpath(selectProduct2));
        selectProduct3.click();
        Thread.sleep(4000);
        
        WebElement addProductList3=driver.findElement(By.xpath(addProductList2));
        addProductList3.click();
        Thread.sleep(4000);
        
        WebElement selectWhishList3=driver.findElement(By.xpath(selectWhishList2));
        selectWhishList3.click();
        Thread.sleep(4000);
        
        WebElement confirmProductList3=driver.findElement(By.xpath(confirmProductList2));
        confirmProductList3.click();
        Thread.sleep(4000);
        
        WebElement myAccount1=driver.findElement(By.xpath(myAccount));
        myAccount1.click();
        Thread.sleep(4000);
        
        WebElement myWhishList1=driver.findElement(By.xpath(myWhishList));
        myWhishList1.click();
        Thread.sleep(4000);
		
        WebElement whishList1=driver.findElement(By.xpath(whishList));
        whishList1.click();
        Thread.sleep(4000);
        
        WebElement addWhishListToCart1=driver.findElement(By.xpath(addWhishListToCart));
        addWhishListToCart1.click();
        Thread.sleep(4000);
        
        WebElement shoppingCart1=driver.findElement(By.xpath(shoppingCart));	   	      
        shoppingCart1.click();
	    Thread.sleep(4000);
	    
	    WebElement viewShopCart1=driver.findElement(By.xpath(viewShopCart));	   	      
        viewShopCart1.click();
	    Thread.sleep(4000);
	    
	    WebElement selectBuyNow1=driver.findElement(By.xpath(selectBuyNow));	   	      
	    selectBuyNow1.click();
	    Thread.sleep(4000);
	    
	    WebElement continueShoping1=driver.findElement(By.xpath(continueShoping));	   	      
	    continueShoping1.click();
	    Thread.sleep(4000);
	    
	    WebElement editQuantity1=driver.findElement(By.xpath(editQuantity));
	    editQuantity1.click();
        Thread.sleep(4000);
        
        WebElement selectQuantity1=driver.findElement(By.xpath(selectQuantity));
        selectQuantity1.clear();
        selectQuantity1.sendKeys("3");
	    Thread.sleep(4000);
	    
	    WebElement updateCart1=driver.findElement(By.xpath(updateCart));
	    updateCart1.click();
	    Thread.sleep(4000);
	    
	    WebElement confirmBuy1=driver.findElement(By.xpath(confirmBuy));	   	      
	    confirmBuy1.click();
	    Thread.sleep(4000);
	    
	    WebElement shippingMethod1=driver.findElement(By.xpath(shippingMethod));
	    shippingMethod1.click();
	    Thread.sleep(4000);
	    
	    WebElement showMap1=driver.findElement(By.xpath(showMap));	   	      
	    showMap1.click();
	    Thread.sleep(4000);
	    
	    WebElement selectStore1=driver.findElement(By.xpath(selectStore));
	    selectStore1.click();
	    Thread.sleep(4000);
	    
	    WebElement submit3=driver.findElement(By.xpath(submit2));
	    submit3.click();
	    Thread.sleep(4000);
	    
	    WebElement paymentMethod1=driver.findElement(By.xpath(paymentMethod));
	    paymentMethod1.click();
	    Thread.sleep(4000);
	    
	    WebElement submitToBuy1=driver.findElement(By.xpath(submitToBuy));
	    submitToBuy1.click();
	    Thread.sleep(4000);
	    
	    driver.close();
           
	}
}
