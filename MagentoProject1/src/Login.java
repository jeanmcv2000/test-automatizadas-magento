import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class Login {

	static WebDriver driver;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\juan.casas\\Pictures\\e-commerce-solution\\MagentoProject1\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo-ecommerce-solution01.southcentralus.cloudapp.azure.com:8082");
	}

	
	@Test
	void login() throws InterruptedException  {
		
		String login="/html/body/div[2]/header/div[1]/div/ul/li[3]/a";
		String email="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[2]/div/input";
		String password="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[3]/div/input";
		String submit="/html/body/div[2]/main/div[3]/div/div[5]/div[1]/div[2]/form/fieldset/div[4]/div[1]/button";
		
		WebElement element=driver.findElement(By.xpath(login));
		element.click();
		Thread.sleep(4000);
		
		WebElement email1=driver.findElement(By.xpath(email));
        email1.clear();
        email1.sendKeys("jeanmcv2000@gmail.com");
        Thread.sleep(4000);
        
        WebElement password1=driver.findElement(By.xpath(password));
        password1.clear();
        password1.sendKeys("admin_01");
        Thread.sleep(4000);
        
        WebElement submit1=driver.findElement(By.xpath(submit));
        submit1.click();
        Thread.sleep(4000);
        
        driver.close();
		
	}

}
