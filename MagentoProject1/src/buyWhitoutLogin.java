import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class buyWhitoutLogin {
	
	static WebDriver driver;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\juan.casas\\Pictures\\e-commerce-solution\\MagentoProject1\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo-ecommerce-solution01.southcentralus.cloudapp.azure.com:8082");
	}
	
	@Test
	void processBuyWithoutLogin() throws InterruptedException  {
	
		
		String category="/html/body/div[2]/div[1]/div/div[2]/nav/ul/li[5]/a";
		String selectProduct="//*[@id=\"layer-product-list\"]/div[2]/ol/li[1]/div/a/span/span/img";
		String addToCart="//*[@id=\"product-addtocart-button\"]";
		String shoppingCart="/html/body/div[2]/header/div[2]/div[1]/a";
		String viewShopCart="//*[@id=\"minicart-content-wrapper\"]/div[2]/div[5]/div/a";
		String selectBuyNow="//*[@id=\"maincontent\"]/div[3]/div/div[4]/div[1]/ul/li[1]/button";
		String customerEmail="//*[@id=\"customer-email\"]";
		String customerName="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[1]/div/input";
		String customerLastName="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[2]/div/input";
		String customerAdress="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/fieldset/div/div[1]/div/input";
		String selectCountry="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[4]/div/select/option[153]";
		String selectState="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[5]/div/select/option[8]";
		String city="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[7]/div/input";
		String zipCode="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[8]/div/input";
		String phoneNumber="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[1]/div[2]/form[2]/div/div[9]/div/input";
		String shippingMethod="/html/body/div/main/div[2]/div/div[4]/div[4]/ol/li[2]/div/div[3]/form/div[1]/table/tbody/tr[2]/td[1]/input";
		String showMap="/html/body/div[1]/main/div[2]/div/div[4]/div[4]/ol/li[2]/div/div[3]/form/div[1]/table/tbody/tr[3]/td/div/p/a";
		String selectStore="//*[@id=\"button-AV-ANDRES-BELLO\"]";
		String submit="//*[@id=\"shipping-method-buttons-container\"]/div/button";
		String paymentMethod="//*[@id=\"cashondelivery\"]";
		String submitToBuy="//*[@id=\"checkout-payment-method-load\"]/div/div/div[3]/div[2]/div[4]/div/button";

		WebElement category1=driver.findElement(By.xpath(category));
		category1.click();
		Thread.sleep(4000);
		
		WebElement selectProduct1=driver.findElement(By.xpath(selectProduct));
        selectProduct1.click();
        Thread.sleep(4000);
        
        WebElement addToCart1=driver.findElement(By.xpath(addToCart));
        addToCart1.click();
        Thread.sleep(4000);
        
        WebElement shoppingCart1=driver.findElement(By.xpath(shoppingCart));	   	      
        shoppingCart1.click();
	    Thread.sleep(4000);
        
        WebElement viewShopCart1=driver.findElement(By.xpath(viewShopCart));	   	      
        viewShopCart1.click();
	    Thread.sleep(4000);
	    
	    WebElement selectBuyNow1=driver.findElement(By.xpath(selectBuyNow));	   	      
	    selectBuyNow1.click();
	    Thread.sleep(4000);
	    
	    WebElement customerEmail1=driver.findElement(By.xpath(customerEmail));	   	      
	    customerEmail1.clear();
	    customerEmail1.sendKeys("manuel@gmail.com");
	    Thread.sleep(4000);
	    
	    WebElement customerName1=driver.findElement(By.xpath(customerName));	   	      
	    customerName1.clear();
	    customerName1.sendKeys("Juan Manuel");
	    Thread.sleep(4000);

	    WebElement customerLastName1=driver.findElement(By.xpath(customerLastName));	   	      
	    customerLastName1.clear();
	    customerLastName1.sendKeys("Casas");
	    Thread.sleep(4000);
	    
	    WebElement customerAdress1=driver.findElement(By.xpath(customerAdress));	   	      
	    customerAdress1.clear();
	    customerAdress1.sendKeys("Yucatan Mz 12 Lt 30");
	    Thread.sleep(4000);
	    
	    WebElement selectCountry1=driver.findElement(By.xpath(selectCountry));	   	      
	    selectCountry1.click();
	    Thread.sleep(4000);
	    
	    WebElement selectState1=driver.findElement(By.xpath(selectState));	   	      
	    selectState1.click();
	    Thread.sleep(4000);
	    
	    WebElement city1=driver.findElement(By.xpath(city));	   	      
	    city1.clear();
	    city1.sendKeys("CDMX");
	    Thread.sleep(4000);
	    
	    WebElement zipCode1=driver.findElement(By.xpath(zipCode));	   	      
	    zipCode1.clear();
	    zipCode1.sendKeys("00200");
	    Thread.sleep(4000);
	    
	    WebElement phoneNumber1=driver.findElement(By.xpath(phoneNumber));	   	      
	    phoneNumber1.clear();
	    phoneNumber1.sendKeys("5510234758");
	    Thread.sleep(4000);
	    
	    WebElement shippingMethod1=driver.findElement(By.xpath(shippingMethod));	   	      
	    shippingMethod1.click();
	    Thread.sleep(4000);
	    
	    WebElement showMap1=driver.findElement(By.xpath(showMap));	   	      
	    showMap1.click();
	    Thread.sleep(4000);
	    
	    WebElement selectStore1=driver.findElement(By.xpath(selectStore));	   	      
	    selectStore1.click();
	    Thread.sleep(4000);
	    
	    WebElement submit1=driver.findElement(By.xpath(submit));	   	      
	    submit1.click();
	    Thread.sleep(4000);
	    
	    WebElement paymentMethod1=driver.findElement(By.xpath(paymentMethod));	   	      
	    paymentMethod1.click();
	    Thread.sleep(4000);
	    
	    WebElement submitToBuy1=driver.findElement(By.xpath(submitToBuy));	   	      
	    submitToBuy1.click();
	    Thread.sleep(4000);
	    
	    driver.close();
	
	}
}
